/* Copyright 2023 Klaas Kliffen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sbus.h"

// SBUS package length = 25 bytes
unsigned static constexpr SBUS_PACKET_SIZE = 25;
// 22 bytes for 16 channels, 1 byte for flags
unsigned static constexpr SBUS_PAYLOAD_SIZE = 23;

uint8_t static constexpr SBUS_START_BYTE = 0x0F;
uint8_t static constexpr SBUS_STOP_BYTE = 0x00;

// Flags
uint8_t static constexpr SBUS_FAILSAFE = 0x10;
uint8_t static constexpr SBUS_LOST_FRAME = 0x04;

// REAL 172
uint16_t static constexpr SBUS_CHANNEL_MIN = 182;
// REAL 1811
uint16_t static constexpr SBUS_CHANNEL_MAX = 1801;


SBUS::SBUS(HardwareSerial *serial)
  : d_serial(serial),
    d_offset(0),
    d_failsafe(false) {}

void SBUS::begin() {
  // Clear buffers before listening
  for (uint8_t idx = 0; idx != SBUS_BUFFER_SIZE; ++idx)
    d_buffer[idx] = 0;

  for (uint8_t idx = 0; idx != SBUS_NUM_CHANNELS; ++idx)
    d_channels[idx] = 0;

  d_serial->begin(100000, SERIAL_8E2);
}


bool SBUS::available() {
  bool readPacket = false;
  while (d_serial->available() > 0) {
    int8_t val = d_serial->read();
    d_buffer[d_offset] = val;
    ++d_offset;
    d_offset %= SBUS_BUFFER_SIZE;
    uint8_t next = (d_offset + SBUS_BUFFER_SIZE - SBUS_PACKET_SIZE) % SBUS_BUFFER_SIZE;

    // Buffer contains a complete packet
    if (val == SBUS_STOP_BYTE && d_buffer[next] == SBUS_START_BYTE)
      readPacket = parsePacket(next);
  }

  return readPacket;
}

int16_t SBUS::getChannelValue(byte channel, byte deadzone) const {
  // Sanity check
  if (channel > SBUS_NUM_CHANNELS - 1)
    return 0;

  int16_t val = constrain(d_channels[channel], SBUS_CHANNEL_MIN, SBUS_CHANNEL_MAX);
  int16_t mapped = map(val, SBUS_CHANNEL_MIN, SBUS_CHANNEL_MAX, -512.0, 512.0);
  return abs(mapped) < deadzone ? 0 : mapped;
}

bool SBUS::failsafe() const {
  // Assumes 0 pulses
  return d_channels[0] == 0 && d_channels[1] == 0;
}

// --- Private -------------------------

inline uint8_t SBUS::buf(uint8_t idx) const {
  return d_buffer[idx % SBUS_BUFFER_SIZE];
}

bool SBUS::parsePacket(uint8_t s) {
  d_channels[0] = (buf(s + 1) | buf(s + 2) << 8) & 0x07FF;
  d_channels[1] = (buf(s + 2) >> 3 | buf(s + 3) << 5) & 0x07FF;
  d_channels[2] = (buf(s + 3) >> 6 | buf(s + 4) << 2 | buf(s + 5) << 10) & 0x07FF;
  d_channels[3] = (buf(s + 5) >> 1 | buf(s + 6) << 7) & 0x07FF;
  d_channels[4] = (buf(s + 6) >> 4 | buf(s + 7) << 4) & 0x07FF;
  d_channels[5] = (buf(s + 7) >> 7 | buf(s + 8) << 1 | buf(s + 9) << 9) & 0x07FF;
  d_channels[6] = (buf(s + 9) >> 2 | buf(s + 10) << 6) & 0x07FF;
  d_channels[7] = (buf(s + 10) >> 5 | buf(s + 11) << 3) & 0x07FF;

  d_channels[8] = (buf(s + 12) | buf(s + 13) << 8) & 0x07FF;
  d_channels[9] = (buf(s + 13) >> 3 | buf(s + 14) << 5) & 0x07FF;
  d_channels[10] = (buf(s + 14) >> 6 | buf(s + 15) << 2 | buf(s + 16) << 10) & 0x07FF;
  d_channels[11] = (buf(s + 16) >> 1 | buf(s + 17) << 7) & 0x07FF;
  d_channels[12] = (buf(s + 17) >> 4 | buf(s + 18) << 4) & 0x07FF;
  d_channels[13] = (buf(s + 18) >> 7 | buf(s + 19) << 1 | buf(s + 20) << 9) & 0x07FF;
  d_channels[14] = (buf(s + 20) >> 2 | buf(s + 21) << 6) & 0x07FF;
  d_channels[15] = (buf(s + 21) >> 5 | buf(s + 22) << 3) & 0x07FF;

  // Returns true, as a shortcut for parsePacket
  return true;
}
