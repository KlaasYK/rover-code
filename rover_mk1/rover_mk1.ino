/* Copyright 2023 Klaas Kliffen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sbus.h"
#include <avr/interrupt.h>

// Timer constants
long static constexpr PRINT_DELAY = 1000;

// Input pins
unsigned static constexpr NUM_MOTORS = 6;

enum CHANNEL: uint8_t {THROTTLE, ROLL, PITCH, YAW, ARM};

// Control pins
// Motor connector                    = { 1,  2,  3,  4,  5,  6};
unsigned static constexpr PWM_PIN[]   = { 5,  4,  3,  2,  6,  7};
unsigned static constexpr BRAKE_PIN[] = {49, 43, 37, 31, 44, 38};
unsigned static constexpr STOP_PIN[]  = {47, 41, 35, 29, 46, 40};
unsigned static constexpr DIR_PIN[]   = {45, 39, 33, 27, 48, 42};

// Global variables
uint8_t static constexpr LED_PIN = 13;
// Receiver on RX3 (no TX!)
SBUS receiver(&Serial3);
// TODO: Look in to FrSky Smart Port

// Encoder pulses
volatile unsigned long encoder[NUM_MOTORS];
volatile uint8_t encoder_state;   // stored state (high/low)

unsigned long last;
long print_timer;
bool led_state = false;

/**
 * Initialize all encoder counters to 0
 */
void initEncoders()
{
  encoder_state = PINK;
  for (uint8_t i = 0; i != NUM_MOTORS; ++i)
    encoder[i] = 0;
}

// Interrupt service routive (A8-A15)
ISR(PCINT2_vect)
{
  uint8_t next = PINK;  // Read all values
  uint8_t change = encoder_state ^ next;
  encoder_state = next;
  // TODO: loop over all encoders
  // TODO: take direction into account?
  if (change & 0x80)
    encoder[0]++;

  if (change & 0x40)
    encoder[1]++;

  if (change & 0x20)
    encoder[2]++;

  if (change & 0x10)
    encoder[3]++;

}


void initMotors()
{
  for (uint8_t i = 0; i != NUM_MOTORS; ++i)
  {

    pinMode(PWM_PIN[i], OUTPUT);
    pinMode(DIR_PIN[i], OUTPUT);
    pinMode(BRAKE_PIN[i], OUTPUT);
    pinMode(STOP_PIN[i], INPUT);      // INPUT leaves pin floating

    analogWrite(PWM_PIN[i], 0);       // Disable motor at startup
    digitalWrite(DIR_PIN[i], HIGH);   // Default direction
    digitalWrite(BRAKE_PIN[i], LOW);  // Disable brakes at startup
    digitalWrite(STOP_PIN[i], LOW);   // LOW disables pullup resistor
  }
}

void setup()
{
  // Setup interrupts
  cli();
  PCICR  |= 0b00000100; // Turn on PinChange interrupts on A8-A15
  PCMSK2 |= 0b11110000; // Mask (enable only A8-A13;
  sei();

  Serial.begin(9600);
  receiver.begin();

  initEncoders();
  initMotors();
  pinMode(LED_PIN, OUTPUT);

  // Setup timers;
  last = 0;
  print_timer = PRINT_DELAY;
}

void stopMotors()
{
  for (uint8_t i = 0; i != NUM_MOTORS; ++i)
  {
    analogWrite(PWM_PIN[i], 0);         // Set speed to 0
    digitalWrite(BRAKE_PIN[i], HIGH);   // Apply the brakes
  }
}

void setSpeed(uint8_t speed)
{
  for (uint8_t i = 0; i != NUM_MOTORS; ++i)
  {
    digitalWrite(BRAKE_PIN[i], LOW);
    analogWrite(PWM_PIN[i], speed);
  }
}

void setDirection(int16_t dir)
{
  if (dir > 0) {
    digitalWrite(DIR_PIN[0], HIGH);   // Default direction
    digitalWrite(DIR_PIN[1], HIGH);   // Default direction
    digitalWrite(DIR_PIN[2], LOW);    // Inverted
    digitalWrite(DIR_PIN[3], LOW);    // Inverted
  } else {
    digitalWrite(DIR_PIN[0], LOW);   // Default direction
    digitalWrite(DIR_PIN[1], LOW);   // Default direction
    digitalWrite(DIR_PIN[2], HIGH);    // Inverted
    digitalWrite(DIR_PIN[3], HIGH);    // Inverted
  }
}

void setMotor(uint8_t motor, uint8_t speed, int16_t dir)
{
  digitalWrite(BRAKE_PIN[motor], LOW);
  digitalWrite(DIR_PIN[motor], dir > 0 ? HIGH : LOW);
  analogWrite(PWM_PIN[motor], speed);
}

void loop()
{
  // Handle timers
  unsigned long now = millis();
  unsigned long delta = now - last;
  last = now;

  print_timer -= delta;
  if (print_timer < 0) {
    print_timer += PRINT_DELAY;
    Serial.println("Telemetry:");
    Serial.print("encoder: ");
    Serial.print(encoder[0]);

    Serial.print("\t");
    Serial.print(encoder[1]);

    Serial.print("\t");
    Serial.print(encoder[2]);

    Serial.print("\t");
    Serial.println(encoder[3]);

    Serial.print("Armed:");
    Serial.println(receiver.getChannelValue(ARM));
    if (receiver.failsafe()) {
      Serial.println("!!! Failsafe !!!");
    }
  }

  if (receiver.available())
  {
    bool armed = receiver.getChannelValue(ARM) > 0;
    digitalWrite(LED_PIN, armed);
    if (!armed || receiver.failsafe())
    {
      stopMotors();
    } else {
      int16_t throttle = receiver.getChannelValue(THROTTLE);
      float fthrottle = throttle / 512.0;
      int16_t steer = receiver.getChannelValue(ROLL);
      float fsteer = fthrottle * (1.0 - abs(steer) / 256.0);

      if (steer > 0) {
        // Reduce speed right
        setMotor(0, abs(fthrottle) * 255, -1 * throttle);
        setMotor(1, abs(fthrottle) * 255, -1 * throttle);
        setMotor(2, abs(fsteer) * 255, fsteer > 0);
        setMotor(3, abs(fsteer) * 255, fsteer > 0);
      } else {
        // Reduce speed Left
        setMotor(0, abs(fsteer) * 255, fsteer < 0);
        setMotor(1, abs(fsteer) * 255, fsteer < 0);
        setMotor(2, abs(fthrottle) * 255, throttle);
        setMotor(3, abs(fthrottle) * 255, throttle);
      }
    }
  }

}
