/* Copyright 2023 Klaas Kliffen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SBUSB_H
#define SBUS_H

#include <Arduino.h>

// Buffer is sligtly larger than packet for faster modulo operations
// Must be power of two
unsigned static constexpr SBUS_BUFFER_SIZE = 32;
unsigned static constexpr SBUS_NUM_CHANNELS = 16;

class SBUS
{
  HardwareSerial *d_serial;
  uint8_t d_offset;
  bool d_failsafe;

  // Ringbuffer and channel storage
  uint8_t d_buffer[SBUS_BUFFER_SIZE];
  uint16_t d_channels[SBUS_NUM_CHANNELS];


  public:
    SBUS(HardwareSerial *serial);

    void begin();

    /**
     * Polls the underlying serial connection
     * and returns whether a valid sbus packet is available
     */
    bool available();

    /**
     * Get the normalised channel value [-512, 512]
     */
    int16_t getChannelValue(uint8_t channel, uint8_t deadzone = 15) const;

    bool failsafe() const;

  private:
    uint8_t buf(uint8_t idx) const;
    bool parsePacket(uint8_t start);


};

#endif // SBUS_H
